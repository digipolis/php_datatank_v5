<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DbdefinitionUpdate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('dbdefinitions', function(Blueprint $table)
        {
            $table->text('description_type_change');    // Create new column with text type
        });

        DB::statement('update dbdefinitions set description_type_change=description');  // Seed column with text type, with data from old column

        Schema::table('dbdefinitions', function(Blueprint $table)
        {
            //
            $table->dropColumn('description');  // drop original column
            $table->renameColumn('description_type_change', 'description'); // rename new column
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('dbdefinitions', function(Blueprint $table)
        {
            $table->char('description_type_change', 255);    // Create new column with text type
        });

        DB::statement('update dbdefinitions set description_type_change=description');  // Seed column with text type, with data from old column

        Schema::table('dbdefinitions', function(Blueprint $table)
        {
            //
            $table->dropColumn('description');  // drop original column
            $table->renameColumn('description_type_change', 'description'); // rename new column
        });
	}

}
