<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuurtmonitorDefinitionsModel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Create a table for buurtmonitordefinitions rdf definition
        Schema::create('buurtmonitordefinitions', function($table){

            $table->increments('id');
            $table->string('uri', 255);
            $table->string('guid', 255);
            $table->string('description', 255);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('buurtmonitordefinitions');
	}

}
