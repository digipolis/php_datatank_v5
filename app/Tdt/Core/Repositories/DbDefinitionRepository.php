<?php
/**
 * Created by PhpStorm.
 * User: Arne
 * Date: 18/04/14
 * Time: 15:44
 */

namespace Tdt\Core\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Tdt\Core\Pager;
use Tdt\Core\Repositories\Interfaces\DbDefinitionRepositoryInterface;
use Tdt\Core\Spectql\implementation\Universalfilters\BinaryFunction;
use Tdt\Core\Spectql\implementation\Universalfilters\Constant;
use Tdt\Core\Spectql\implementation\Universalfilters\FilterByExpressionFilter;
use Tdt\Core\Spectql\implementation\Universalfilters\Identifier;
use Tdt\Core\Spectql\source\SPECTQLParser;

class DbDefinitionRepository extends TabularBaseRepository implements DbDefinitionRepositoryInterface
{
    protected $rules = array(
        'table' => 'required',
    );

    /**
     * Name of the Database.
     *
     * @var string
     */
    protected $connectionName   =   'odataetl_logging';

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;

    /**
     * @param \DbDefinition $model
     */
    public function __construct(\DbDefinition $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    /**
     * @param int $id
     * @param array $input
     * @return array
     */
    public function update($id, array $input)
    {
        return parent::update($id, $input);
    }

    /**
     * @param array $input
     * @return array
     */
    public function store(array $input)
    {
        return parent::store($input);
    }

    /**
     * Retrieve the set of create parameters that make up a CSV definition.
     * Include the parameters that make up relationships with this model.
     */
    public function getAllParameters()
    {
        $column_params = array(
            'columns' =>
                array(
                    'description' => 'Columns must be an array of objects of which the template is described in the parameters section.',
                    'parameters' => $this->tabular_repository->getCreateParameters(),
                )
        );

        $geo_params = array(
            'geo' =>
                array(
                    'description' => 'Geo must be an array of objects of which the template is described in the parameters section.',
                    'parameters' => $this->geo_repository->getCreateParameters(),
                )
        );

        return array_merge($this->getCreateParameters(), $column_params, $geo_params);
    }

    /**
     * Extracts the column names from a table
     * Also sets the column names array in this class.
     *
     * @param string $table
     * @return array
     */
    public function getColumns($table)
    {
        $query                  = 'SHOW COLUMNS FROM '.$table;
        $columnNameParameter    = 'Field';
        $columns                = array();

        foreach($this->getConnection()->select($query) as $column)
        {
            $columns[] = $column->$columnNameParameter;;
        }

        return $columns;
    }

    /**
     * @param $config
     * @return array
     */
    public function extractColumns($config)
    {
        $columnNames = $this->getColumns($config['table']);
        $columns                = array();
        $index                  = -1;

        foreach($columnNames as $column)
        {
            $column = array(
                "index" => ++$index,
                "column_name" => $column,
                "column_name_alias" => $column,
                "is_pk" => ($column == @$config['pk']),
            );

            $columns[] = $column;
        }

        return $columns;
    }

    /**
     * Reads all the data from the table
     *
     * @param $tableName
     * @param array $restParams
     * @return Array $data
     */
    public function get($tableName, $restParams = array())
    {
        $query = $this->getConnection()->table($tableName);

        self::addQueryFilters($query, $tableName, $restParams);

        //echo $query->toSql();die();
        //var_dump($query->get());die();
        return $query->get();
    }

    /**
     * Get total rows from table
     * @param $tableName
     * @param array $restParams
     * @return int
     */
    public function getTotalRows($tableName, $restParams = array())
    {
        $query = $this->getConnection()->table($tableName);
        self::addQueryFilters($query, $tableName, $restParams);
        $query->take(99999999); // because addQueryFilters adds limit & offset params
        $query->skip(0);
        return $query->count();;
    }

    /**
     * Adds QueryString filters to the query
     *
     * @param Builder $query
     * @param $tableName
     * @param array $restParams
     */
    protected function addQueryFilters(Builder &$query, $tableName,  $restParams = array())
    {
        $params  = \Input::all();
       /*
        * When a user is using SpectQL :
        * I want to use the SpectQL parameters to filter on SQL level, not php level.
        */

        if (strpos($requestUri = \Request::fullUrl(),'spectql') == false){
            list($limit, $offset)   = Pager::calculateLimitAndOffset();
            // No SpectQL, simple filtering.
            $field      = null;

            foreach($restParams as $key => $param){
                if($key % 2 == 0){
                    $field = $param;
                }else{
                    $params[$field] = $param;
                }
            }

            if($params){
                foreach($params  as $field => $value){
                    if(in_array($field, $this->getColumns($tableName)))
                        $query->where($field, 'LIKE', str_replace('*', '%',$value));
                }
            }

        }else{
            $parser                 = new SPECTQLParser(self::getRequestUriForSpectQLQuery());
            $context                = array(); // array of context variables
            $spectQlQuery           = $parser->interpret($context);
            list($limit, $offset)   = Pager::calculateLimitAndOffsetFromRestParams($restParams);
            self::parseColumnSelectors($spectQlQuery->getColumnData(), $query);
            self::parseDataFilters($spectQlQuery->getSource(), $query);
        }

        // Only paginate if the format is not .csv or .xml, thus only paginate .map / .json and others
        if(false === strpos(\Request::getPathInfo(), '.csv')){
            if($offset) {
                $query->skip($offset);
            }

            if($limit) {
                $query->take($limit);
            }
        }else{
            $query->skip(0);
            $query->take(20000);
        }


    }

    /**
     * @param array $columnSelectors
     * @param Builder $query
     * @return Builder
     */
    protected function parseColumnSelectors(array $columnSelectors, Builder &$query)
    {
        foreach($columnSelectors as $columnSelector){
            /**
             * @var $columnSelector \Tdt\Core\Spectql\implementation\Universalfilters\ColumnSelectionFilterColumn
             * @var $column  \Tdt\Core\Spectql\implementation\Universalfilters\Identifier
             */
            $column     = $columnSelector->getColumn();
            if($column instanceof Identifier)
                $query->addSelect($column->getIdentifierString());
        }

        return $query;
    }

    /**
     * @param mixed $dataFilters
     * @param Builder $query
     */
    protected function parseDataFilters($dataFilters, Builder &$query)
    {
        if(!$dataFilters instanceof FilterByExpressionFilter)
            return;

        if(!($expression = $dataFilters->getExpression()) instanceof BinaryFunction)
            return;

        /** @var $expression BinaryFunction */
        self::parseBinaryFunction($expression, $query);
    }

    /**
     * @param BinaryFunction $function
     * @param Builder $query
     * @param bool $apply Either apply the parameters included in the binary function, or return them as an array
     * @return array
     */
    protected function parseBinaryFunction(BinaryFunction $function, Builder &$query, $apply = true)
    {
        $identifier = false;
        $constant   = false;

        if($function->getType() == BinaryFunction::$FUNCTION_BINARY_OR){
            // Add where statements of first query
            self::parseBinaryFunction($function->getSource(0), $query);
            // get field / value from second OR statement
            list($identifier, $constant) = self::parseBinaryFunction($function->getSource(1), $query, FALSE);
            // Add OR query for second query thingy
            $query->orWhere($identifier, self::getOperatorForBinaryFunctionType($function->getSource(1)->getType()), $constant);
        }elseif($function->getType() == BinaryFunction::$FUNCTION_BINARY_AND){
            foreach($function->getSourceArray() as $expression){
                // This will just add more 'where's, which means AND statements
                self::parseBinaryFunction($expression, $query, TRUE);
            }
        }else{
            foreach($function->getSourceArray() as $expression){
                if($expression instanceof Identifier || $expression instanceof Constant){
                    if($expression->getType() == 'CONSTANT')
                        $constant = $expression->getConstant();

                    if($expression->getType() == 'IDENTIFIER')
                        $identifier = $expression->getIdentifierString();
                }
            }

            if($identifier && $constant && $apply){
                if($function->getType() == BinaryFunction::$FUNCTION_BINARY_MATCH_REGEX){
                    $constant = "%$constant%";
                    $constant = str_replace('/.*',  '', $constant);
                    $constant = str_replace('.*/',  '', $constant);
                }

                // Skip the filtering if there's no need known operator for the method.
                if($operator = self::getOperatorForBinaryFunctionType($function->getType()))
                    $query->where($identifier, $operator, $constant);
            }
        }

        return array($identifier, $constant);
    }

    private function getOperatorForBinaryFunctionType($type)
    {
        if($type == BinaryFunction::$FUNCTION_BINARY_COMPARE_EQUAL)
           return '=';

        if($type == BinaryFunction::$FUNCTION_BINARY_COMPARE_LARGER_OR_EQUAL_THAN)
            return '>=';

        if($type == BinaryFunction::$FUNCTION_BINARY_COMPARE_LARGER_THAN)
            return '>';

        if($type == BinaryFunction::$FUNCTION_BINARY_COMPARE_SMALLER_OR_EQUAL_THAN)
            return '<=';

        if($type == BinaryFunction::$FUNCTION_BINARY_COMPARE_SMALLER_THAN)
            return '<';

        if($type == BinaryFunction::$FUNCTION_BINARY_MATCH_REGEX)
            return 'like';

        if($type == BinaryFunction::$FUNCTION_BINARY_COMPARE_NOTEQUAL)
            return '!=';

        return false;
    }

    /**
     * a spectQL function that fetches the correct request_uri from the full request url.
     * @return mixed|string
     */
    private function getRequestUriForSpectQLQuery()
    {
        $original_uri   = \Request::fullUrl();
        $root           = \Request::root();
        $query_uri      = "";

        if (preg_match("%$root\/spectql\/(.*)%", $original_uri, $matches)) {
            $query_uri = urldecode($matches[1]);
        }

        $format = "";

        // Fetch the format of the query
        if (preg_match("/.*(:[a-zA-Z]+)&?(.*?)/", $query_uri, $matches)) {
            $format = ltrim($matches[1], ":");
        }

        // Remove the format and any following query string parameters
        if (!empty($format)) {
            $query_uri = preg_replace("/:" . $format . "\??.*/", '', $query_uri);
        }

        return $query_uri;
    }

    /**
     * Return the properties (= column fields) for this model.
     */
    public function getCreateParameters()
    {
        return array(
            'table' => array(
                'required' => true,
                'name' => 'Table Name',
                'description' => 'Defines in which table the data is found.',
                'type' => 'string',
            ),
            'description' => array(
                'required' => false,
                'name' => 'Description',
                'description' => 'A Description of the dataset.',
                'type' => 'text',
            ),
        );
    }


    /**
     * @param string $connectionName
     * @return $this
     */
    public function setConnectionName($connectionName)
    {
        $this->connectionName = $connectionName;
        return $this;
    }

    /**
     * @return string
     */
    public function getConnectionName()
    {
        return $this->connectionName;
    }

    /**
     * @param \Illuminate\Database\Connection $connection
     * @return $this
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;
        return $this;
    }

    /**
     * @return \Illuminate\Database\Connection
     */
    public function getConnection()
    {
        if(!$this->connection)
            $this->setConnection(\DB::connection($this->getConnectionName()));

        return $this->connection;
    }
}
