<?php
/**
 * Created by PhpStorm.
 * User: Arne
 * Date: 18/04/14
 * Time: 12:02
 */

namespace Tdt\Core\Repositories;

use Tdt\Core\Repositories\Interfaces\BuurtmonitorDefinitionRepositoryInterface;

class BuurtmonitorDefinitionRepository extends BaseDefinitionRepository implements BuurtmonitorDefinitionRepositoryInterface
{
    protected $rules = array(
        'guid' => 'required',
        'uri' => 'required',
        'description' => 'required',
    );

    public function __construct(\BuurtmonitorDefinition $model)
    {
        $this->model = $model;
    }

    /**
     * Retrieve the set of create parameters that make up a installed definition.
     */
    public function getCreateParameters()
    {
        return array(
            'uri' => array(
                'required' => true,
                'name' => 'URI',
                'description' => 'Location of the WSDL File',
                'type' => 'string',
            ),
            'guid' => array(
                'required' => true,
                'name' => 'GUID',
                'description' => 'GUID Of the SWING report.',
                'type' => 'string',
            ),
            'description' => array(
                'required' => true,
                'name' => 'Description',
                'description' => 'The descriptive or informational string that provides some context for you published dataset.',
                'type' => 'string',
            )
        );
    }
} 