<?php
/**
 * Created by PhpStorm.
 * User: Arne
 * Date: 18/04/14
 * Time: 12:00
 */

namespace Tdt\Core\Repositories\Interfaces;


interface BuurtmonitorDefinitionRepositoryInterface
{
    /**
     * Return all BuurtmonitorDefinition objects
     *
     * @return array
     */
    public function getAll();

    /**
     * Return a validator based on an hash array
     *
     * @param array $input
     * return Illuminate\Validation\Validator
     */
    public function getValidator(array $input);

    /**
     * Return an array of create parameters with info attached
     * e.g. array( 'create_parameter' => array(
     *              'required' => true,
     *              'description' => '...',
     *              'type' => 'string',
     *              'name' => 'pretty name'
     *       ), ...)
     *
     * @return array
     */
    public function getCreateParameters();

    /**
     * Return an array of all the create parameters, also the parameters
     * that are necessary for further internal relationships
     *
     * @return array
     */
    public function getAllParameters();

    /**
     * Store an BuurtmonitorDefinition
     *
     * @param array $input
     * @return array BuurtmonitorDefinition
     */
    public function store(array $input);


    /**
     * Update an BuurtmonitorDefinition
     *
     * @param integer $Buurtmonitor_id
     * @param array $input
     * @return array BuurtmonitorDefinition
     */
    public function update($Buurtmonitor_id, array $input);

    /**
     * Delete an BuurtmonitorDefinition
     *
     * @param integer $Buurtmonitor_id
     * @return boolean|null
     */
    public function delete($Buurtmonitor_id);

    /**
     * Fetch an BuurtmonitorDefinition by id
     *
     * @param integer $Buurtmonitor_id
     * @return array BuurtmonitorDefinition
     */
    public function getById($Buurtmonitor_id);
} 