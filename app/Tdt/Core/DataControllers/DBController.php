<?php
/**
 * Created by PhpStorm.
 * User: Arne
 * Date: 18/04/14
 * Time: 15:58
 */

namespace Tdt\Core\DataControllers;

use Tdt\Core\Datasets\Data;
use Tdt\Core\Pager;
use Tdt\Core\Repositories\Interfaces\TabularColumnsRepositoryInterface;
use Tdt\Core\Repositories\Interfaces\GeoPropertyRepositoryInterface;

class DBController extends ADataController
{
    /**
     * @var \Tdt\Core\Repositories\Interfaces\TabularColumnsRepositoryInterface
     */
    protected $_tabularColumns;

    /**
     * @var \Tdt\Core\Repositories\Interfaces\GeoPropertyRepositoryInterface
     */
    protected $_geoProperties;

    /**
     * @var \Tdt\Core\Repositories\DbDefinitionRepository
     */
    protected $_dbDefinitionRepository;

    public function __construct(TabularColumnsRepositoryInterface $tabularColumns, GeoPropertyRepositoryInterface $geoProperties)
    {
        $this->setTabularColumns($tabularColumns);
        $this->setGeoProperties($geoProperties);
    }

    /**
     * Reads the data from the correct table in the ETL database.
     *
     * @param $source_definition
     * @param array $rest_parameters
     * @return Data
     */
    public function readData($source_definition, $rest_parameters = array())
    {
        list($limit, $offset)   = Pager::calculateLimitAndOffset();

        if (strpos($requestUri = \Request::fullUrl(),'spectql'))
            list($limit, $offset)   = Pager::calculateLimitAndOffsetFromRestParams($rest_parameters);

        $data                   = self::getDbDefinitionRepository()->get($source_definition['table'],  $rest_parameters);
        $paging                 = Pager::calculatePagingHeaders($limit, $offset, $total = self::getDbDefinitionRepository()->getTotalRows($source_definition['table'], $rest_parameters));
        $dataResult             = new Data();
        $dataResult->data       = $data;
        $dataResult->paging     = $paging;

        return $dataResult;
    }

    /**
     * Parse the columns from a CSV file and return them
     * Optionally aliases can be given to columns as well as a primary key
     */
    public static function parseColumns($config)
    {
        $dbRepo = self::getDbDefinitionRepository();
        return $dbRepo->extractColumns($config);
    }

    /**
     * @param \Tdt\Core\Repositories\Interfaces\GeoPropertyRepositoryInterface $geoProperties
     * @return $this
     */
    public function setGeoProperties($geoProperties)
    {
        $this->_geoProperties = $geoProperties;
        return $this;
    }

    /**
     * @return \Tdt\Core\Repositories\Interfaces\GeoPropertyRepositoryInterface
     */
    public function getGeoProperties()
    {
        return $this->_geoProperties;
    }

    /**
     * @param \Tdt\Core\Repositories\Interfaces\TabularColumnsRepositoryInterface $tabularColumns
     * @return $this
     */
    public function setTabularColumns($tabularColumns)
    {
        $this->_tabularColumns = $tabularColumns;
        return $this;
    }

    /**
     * @return \Tdt\Core\Repositories\Interfaces\TabularColumnsRepositoryInterface
     */
    public function getTabularColumns()
    {
        return $this->_tabularColumns;
    }

    /**
     * @param \Tdt\Core\Repositories\DbDefinitionRepository $dbDefinitionRepository
     * @return $this
     */
    public function setDbDefinitionRepository($dbDefinitionRepository)
    {
        $this->_dbDefinitionRepository = $dbDefinitionRepository;
        return $this;
    }

    /**
     * @return \Tdt\Core\Repositories\DbDefinitionRepository
     */
    public function getDbDefinitionRepository()
    {
        if(!$this->_dbDefinitionRepository)
            $this->setDbDefinitionRepository(\App::make('Tdt\\Core\\Repositories\\DbDefinitionRepository'));

        return $this->_dbDefinitionRepository;
    }
} 
