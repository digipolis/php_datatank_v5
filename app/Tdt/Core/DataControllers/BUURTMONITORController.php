<?php
/**
 * Created by PhpStorm.
 * User: Arne
 * Date: 18/04/14
 * Time: 12:14
 */

namespace Tdt\Core\DataControllers;

use Tdt\Core\Datasets\Data;
use Tdt\Core\Pager;

class BUURTMONITORController
{
    /**
     * GUID Used to fetch SWING data
     * @var String
     */
    protected $_guid;

    /**
     * URI of the WSDL File used to make the soap request
     * @var String
     */
    protected $_wsdlUri;

    /**
     * SoapClient used to request the SWING data from Buurtmonitor
     * @var /SoapClient
     */
    protected $_soapClient;

    /**
     * XML Result object from the Swing Service
     * @var /SimpleXML
     */
    protected $_selectionByGuiResultXml;

    /**
     * String Representing the function name called in the Swing Service
     * @var String SOAP_FUNCTION
     */
    const SOAP_FUNCTION = 'SelectionXmlByGuid';


    public function readData($source_definition, $rest_parameters = array())
    {
        if(isset($source_definition['uri']) && isset($source_definition['guid'])){
            $this->setGuid($source_definition['guid']);
            $this->setWsdlUri($source_definition['uri']);

            $this->_initSoapClient();
            $this->_getSwingData();
            return $this->_parseSwingData();
        }else{
            \App::abort(500, "The GUID and / or WSDL uri parameters are missing. This is most likely due to a corrupt Buurtmonitor Definition");
        }
    }

    protected function _initSoapClient()
    {
        $soapClient = new \SoapClient($this->getWsdlUri());
        $this->setSoapClient($soapClient);
    }

    protected function _getSwingData()
    {
        $soapClient = $this->getSoapClient();
        $result = $soapClient->__soapCall(self::SOAP_FUNCTION, array(array('guid' => $this->getGuid())));
        $selectionXmlString = $result->SelectionXmlByGuidResult;

        $selectionByGuiResultXml = new \SimpleXMLElement($selectionXmlString);

        $this->setSelectionByGuiResultXml($selectionByGuiResultXml);
    }

    protected function _parseSwingData()
    {
        $dataRows = array();
        $xml = $this->getSelectionByGuiResultXml();

        // get the dimensions
        $arr_dimensions = array (); // hierarchy of all dimensions with values
        // with name
        // index
        $arr_dimension_keys = array (); // hierarchy of all dimensions with values
        // with
        // key index
        $arr_dimension_all_code_names = array (); // array with keymapping between
        // code
        // and
        // name

        $arr_dimension_all_code_columnkey = array (); // array with keymapping
        // between code and column name

        foreach ( $xml->xpath ( "/result/dimensions/dimension" ) as $obj_node ) {
            $arr_attributes = ( array ) $obj_node->attributes ();
            $code = $arr_attributes ["@attributes"] ['code'];
            $key_name = strtolower ( ( string ) $obj_node->name );
            $arr_dimensions [$key_name] ['name'] = ( string ) $obj_node->name;
            $arr_dimensions [$key_name] ['name_key'] = $key_name;
            foreach ( $obj_node->member as $obj_dimitem ) {
                $arr_attributes = ( array ) $obj_dimitem->attributes ();
                $code = $arr_attributes ["@attributes"] ['code'];
                $arr_dimensions [$key_name] ['values'] [$code] = ( string ) $obj_dimitem->name;
                $arr_dimension_all_code_names [$code] = ( string ) $obj_dimitem->name;
                $arr_dimension_all_code_columnkey [$code] = $arr_dimensions [$key_name] ['name_key'];
            }
        }
        $arr_dimension_keys = array_values ( $arr_dimensions );

        // get the dimensions
        $arr_headermapping = array (); // keeps track of cellid and
        // multidimensional
        // get the data
        $int_counter_row = 1;
        $int_counter_element = 0;
        $int_counter_headerleft = 0;

        $columnname_previous = "";

        $arr_xmlrows = $xml->xpath ( "/result/data/table/row" );
        $int_number_of_rows = count ( $arr_xmlrows );
        foreach ( $arr_xmlrows as $obj_xmlrow ) {
            $obj_data = new  \StdClass ();
            switch (true) {
                case $int_counter_row == 1 :
                    $int_counter_headerleft = 0;
                    $int_counter_element = 0;
                    foreach ( $obj_xmlrow->children () as $obj_column ) {
                        $arr_attributes = null;
                        $code = null;
                        if (isset ( $obj_column->member )) {
                            $arr_attributes = ( array ) $obj_column->member->attributes ();
                            $code = $arr_attributes ['@attributes'] ['code'];
                        }
                        if (isset ( $code )) {
                            $arr_headermapping [1] [$int_counter_element] = $arr_dimension_all_code_names [$code];
                        } else {
                            $arr_headermapping [1] [$int_counter_element] = "";
                            $int_counter_headerleft ++;
                        }
                        $int_counter_element ++;
                    }
                    break;
                case $int_counter_row == 2 && $int_counter_headerleft == 1 :

                    $int_counter_element = 0;
                    foreach ( $obj_xmlrow->children () as $obj_column ) {
                        $arr_headermapping [2] [$int_counter_element] = ( string ) $obj_column->period->code;
                        $int_counter_element ++;
                    }
                    break;
                default :

                    // read each datarecord into $arr_row_data
                    $arr_row_data = array ();
                    $int_counter_element = 0;
                    $arr_header_codes = array ();
                    foreach ( $obj_xmlrow->children () as $obj_column ) {
                        switch (strtolower ( $obj_column->getName () )) {
                            case "header" :
                                $arr_attributes = null;
                                if (isset ( $obj_column->member )) {
                                    $arr_attributes = ( array ) $obj_column->member->attributes ();
                                }
                                $arr_header_codes [] = $arr_attributes ['@attributes'] ['code'];
                                break;
                            case "cell" :
                                $arr_attributes="";
                                $arr_attributes = $obj_column->attributes ();
                                if(isset($arr_attributes['svt'])){
                                    if($arr_attributes['svt'] == "nvt"){
                                        $arr_row_data [$int_counter_element] = "-";
                                    }elseif($arr_attributes['svt'] == "nt"){
                                        $arr_row_data [$int_counter_element] = "x";
                                    }elseif($arr_attributes['svt'] == "toga"){
                                        $arr_row_data [$int_counter_element] = "?";
                                    }else{
                                        $arr_row_data [$int_counter_element] = ( string ) $obj_column;
                                    }

                                }else{
                                    $arr_row_data [$int_counter_element] = ( string ) $obj_column;
                                }
                                break;
                        }
                        $int_counter_element ++;
                    }

                    if (count ( $arr_dimensions ) > 2) {
                        if ($int_counter_headerleft != 1) {
                            // multicolumn header layer on the left
                            /*
                             * foreach($arr_header_codes as $index => $code ){
                             * $columnname =
                             * $arr_dimension_all_code_columnkey[$code];
                             * $obj_data->$columnname =
                             * $arr_dimension_all_code_names
                             * [$arr_header_codes[$index]]; } foreach (
                             * $arr_headermapping [1] as $index => $value ) { if
                             * ($value != "") { $columnname =
                             * "cell_".strtolower($value);
                             * $obj_data->$columnname = $arr_row_data[$index]; }
                             * }
                             */
                            /*remap */
                            $columnname_first = $arr_header_codes [0]; // subject
                            $columnname_second = $arr_header_codes [1]; // year

                            if (($columnname_previous != "") && (($columnname_first != $columnname_previous) || $int_number_of_rows == $int_counter_row)) {
                                // TODO add check for last row

                                if ($int_number_of_rows == $int_counter_row) {
                                    $columnname_previous = $columnname_first;
                                    foreach ( $arr_headermapping [1] as $index => $value ) {
                                        if ($value != "") {
                                            $arr_data_tmp [$columnname_second] [$value] = $arr_row_data [$index];
                                        }
                                    }
                                }
                                foreach ( $arr_dimension_keys [0] ['values'] as $key => $code ) {
                                    $obj_data_tmp = new stdClass ();
                                    // gebied
                                    $columnname = $arr_dimension_keys [0] ['name_key'];
                                    $obj_data_tmp->$columnname = $code;
                                    // onderwerk
                                    $columnname = $arr_dimension_all_code_columnkey [$columnname_first];
                                    $obj_data_tmp->$columnname = $arr_dimension_all_code_names [$columnname_previous];

                                    foreach ( $arr_data_tmp as $k => $arr_value ) {
                                        $columnname = "data_" . $arr_dimension_all_code_names [$k];

                                        $value = $arr_value [$code];
                                        if(is_null($value)|| $value == ""){
                                            $value = 0;
                                        }
                                        $value = str_replace(".", ",",$value);
                                        $obj_data_tmp->$columnname = $value;
                                    }
                                    $dataRows [] = $obj_data_tmp;
                                }
                                $arr_data_tmp = array ();
                            }

                            $columnname_previous = $columnname_first;
                            foreach ( $arr_headermapping [1] as $index => $value ) {
                                if ($value != "") {
                                    $arr_data_tmp [$columnname_second] [$value] = $arr_row_data [$index];
                                }
                            }

                            /* remap */

                            // $dataRows [] = $obj_data;
                        } else {
                            // 2layer columns on top

                            // firstcolumn
                            $columnname = $arr_dimension_keys [0] ['name_key'];
                            if($arr_header_codes [0] <> ""){
                                $obj_data->$columnname = $arr_dimension_all_code_names [$arr_header_codes [0]];
                            }else{
                                //total column has been reached
                                break;
                            }

                            // multidimensional array
                            $columnname_1 = $arr_dimension_keys [1] ['name_key'];

                            $arr_value_2 = array ();
                            foreach ( $arr_headermapping [1] as $index => $value_1 ) { // loop
                                // over
                                // subject
                                if ($value_1 != "") {
                                    // $arr_value_2
                                    $columnname_2 = $arr_headermapping [2] [$index];

                                    if (isset ( $arr_row_data [$index] )) {
                                        $arr_value_2 [$columnname_2] = $arr_row_data [$index];
                                    }

                                    if ((isset ( $arr_headermapping [1] [$index + 1] ) && $arr_headermapping [1] [$index + 1] != $value_1) || ! isset ( $arr_headermapping [1] [$index + 1] )) {

                                        $obj_data_tmp = clone $obj_data;
                                        $obj_data_tmp->$columnname_1 = $value_1;
                                        foreach ( $arr_value_2 as $key_2 => $value_2 ) {

                                            if (is_integer ( $key_2 )) {
                                                $key_2 = "data_$key_2"; // otherwise integer
                                                // in xml tag
                                            }elseif($key_2 == ""){
                                                $key_2 = 'data';
                                            }

                                            if(is_null($value_2)|| $value_2 == ""){
                                                $value_2 = 0;
                                            }
                                            $value_2 = str_replace(".", ",",$value_2);

                                            $obj_data_tmp->$key_2 = $value_2;
                                        }
                                        $arr_value_2 = array ();

                                        $dataRows [] = $obj_data_tmp;
                                    }
                                }
                            }
                        }
                    } else {
                        // $columnname = $arr_dimension_keys[1]['name_key'];
                        foreach ( $arr_headermapping [1] as $index => $value_1 ) { // loop
                            // over
                            // year
                            if ($value_1 != "") {
                                $obj_data->$value_1 = $arr_row_data [$index];
                            }
                        }
                        $dataRows [] = $obj_data;
                    }

                    break;
            }
            $int_counter_row ++;
        }

        list($limit, $offset) = Pager::calculateLimitAndOffset();
        $paging = Pager::calculatePagingHeaders($limit, $offset, count($dataRows));
        $dataResult = new Data();
        $dataResult->data = array('data'  => $dataRows);
        $dataResult->paging = $paging;
        return $dataResult;
    }

    /**
     * @param String $guid
     * @return $this
     */
    public function setGuid($guid)
    {
        $this->_guid = $guid;
        return $this;
    }

    /**
     * @return String
     */
    public function getGuid()
    {
        if($this->_guid == null){
            \App::abort(500, 'The GUID is not set in the BUURTMONITORController. This is most likely due to a corrupt Buurtmonitor Definition');
        }
        return $this->_guid;
    }

    /**
     * @param String $wsdlUri
     * @return $this
     */
    public function setWsdlUri($wsdlUri)
    {
        $this->_wsdlUri = $wsdlUri;
        return $this;
    }

    /**
     * @return String
     */
    public function getWsdlUri()
    {
        if($this->_wsdlUri == null){
            \App::abort(500, 'The WSDL_URI is not set in the BUURTMONITORController. This is most likely due to a corrupt Buurtmonitor Definition');
        }
        return $this->_wsdlUri;
    }

    /**
     * @param \SoapClient $soapClient
     * @return $this
     */
    public function setSoapClient($soapClient)
    {
        $this->_soapClient = $soapClient;
        return $this;
    }

    /**
     * @return \SoapClient
     */
    public function getSoapClient()
    {
        return $this->_soapClient;
    }

    /**
     * @param mixed $selectionByGuiResultXml
     * @return $this
     */
    public function setSelectionByGuiResultXml($selectionByGuiResultXml)
    {
        $this->_selectionByGuiResultXml = $selectionByGuiResultXml;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSelectionByGuiResultXml()
    {
        if(!$this->_selectionByGuiResultXml instanceof \SimpleXMLElement)
        {
            \App::abort(500, 'Something went wrong fetching the XML Result from the Swing Service');
        }
        return $this->_selectionByGuiResultXml;
    }

} 