<?php

namespace Tdt\Core\Formatters;
/**
 * KML Formatter
 *
 * @copyright (C) 2011, 2014 by OKFN Belgium vzw/asbl
 * @license AGPLv3
 * @author Michiel Vancoillie <michiel@okfn.be>
 * @author Jan Vansteenlandt <jan@okfn.be>
 * @author Pieter Colpaert   <pieter@irail.be>
 */
class KMLFormatter implements IFormatter
{

    private static $LONGITUDE_PREFIXES = array('long', 'lon', 'longitude', 'lng', 'point_lng', 'point_long', 'gisx', 'geo_lon', 'geo_lng' );
    private static $LATITUDE_PREFIXES = array('lat', 'latitude', 'point_lat', 'gisy', 'geo_lat');
    private static $COORDINATES_PREFIXES = array('coordinates', 'polygone', 'polygon', 'geometry');

    protected static $_coordsKey;
    protected static $_latKey;
    protected static $_lngKey;

    public static function createResponse($dataObj)
    {
        // Create response
        $response = \Response::make(self::getBody($dataObj), 200);

        // Set headers
        $response->header('Content-Type', 'application/vnd.google-earth.kml+xml;charset=UTF-8');
        //$response->header('Content-Type', 'application/xml;charset=UTF-8');
        return $response;
    }

    public static function getBody($dataObj)
    {
        // Build the body
        // KML header
        $body = '<?xml version="1.0" encoding="UTF-8" ?>';
        $body .= '<kml xmlns="http://www.opengis.net/kml/2.2">';

        // Add the document
        $body .= "<Document>";

        $body .= self::getPlacemarks($dataObj);

        // Close tags
        $body .= "</Document>";
        $body .= "</kml>";

        return $body;
    }

    private static function getPlacemarks($dataObj)
    {

        $data = $dataObj->data;
        if (is_object($data)) {
            $data = get_object_vars($data);
        }

        // If no geo property is given, don't bother creating a KML
        if (empty($dataObj->geo)) {

            $placemarks = "";
            ob_start();
            self::printArray($dataObj->data, $placemarks);
            $placemarks = ob_get_contents();
            ob_end_clean();

            return $placemarks;
        }

        return self::getArray($dataObj, $dataObj->geo);
    }

    private static function xmlgetelement($value)
    {
        $data = "";
        if(is_array($value)){
            foreach($value as $title => $name){
                if($title != self::getLatKey() && $title != self::getLngKey() && $title !=  self::getCoordsKey()){
                    if(is_object($title) || is_object($name))
                        continue;
                    $data .= '<strong>'.$title.'</strong> : '.$name;
                    $data .= " <br/> ";
                }
            }
        }
        $result = "<![CDATA[";
        $result .= $data;
        $result .= "]]>";
        return $result;
    }

    private static function getExtendedDataElement($value)
    {
        // TODO decide what to do with extended data element
        $result = "<ExtendedData>";
        if (is_object ( $value )) {
            $array = get_object_vars ( $value );
            $result .= self::getDescription($array);
        } else if (is_array ( $value )) {
            $result .= self::getDescription($value);
        } else {
            $result .= htmlspecialchars ( $value );
        }
        $result .= "</ExtendedData>";
        return $result;
    }

    protected static function getDescription($array)
    {
        $result = "";

        foreach ( $array as $key => $val ) {
            if(is_object($key) || is_object($val))
                continue;

            if (is_numeric ($key )) {
                $key = "int_" . $key;
            }
            $key = htmlspecialchars ( str_replace ( " ", "", $key ) );
            $val = htmlspecialchars ( $val );
            $result .= '<Data name="' . $key . '"><value>' . $val . '</value></Data>';
        }

        return $result;
    }

    private static function printArray($val, $placemarks)
    {
        foreach ($val as $key => $value) {

            $long = "";
            $lat = "";
            $coords = array();

            if (is_array($value)) {
                $array = $value;
            }
            if (is_object($value)) {
                $array = get_object_vars($value);
            }

            if (!empty($array)) {

                $longkey = false;
                $latkey = false;
                $coordskey = false;

                foreach (self::$LONGITUDE_PREFIXES as $prefix) {

                    $longkey = self::keyExists($prefix, $array);

                    if ($longkey) {
                        self::setLngKey($longkey);
                        break;
                    }
                }

                foreach (self::$LATITUDE_PREFIXES as $prefix) {

                    $latkey = self::keyExists($prefix, $array);

                    if ($latkey) {
                        self::setLatKey($latkey);
                        break;
                    }
                }

                foreach (self::$COORDINATES_PREFIXES as $prefix) {

                    $coordskey = self::keyExists($prefix, $array);

                    if ($coordskey) {
                        self::setCoordsKey($coordskey);
                        break;
                    }
                }

                if ($longkey && $latkey) {

                    $long = $array[$longkey];
                    $lat = $array[$latkey];

                    unset($array[$longkey]);
                    unset($array[$latkey]);

                    $name = self::xmlgetelement($array);
                    $extendeddata = self::getExtendedDataElement($array);
                } elseif ($coordskey) {
                    $obj_geometry = new \stdClass ();
                    $arr_coordinates = array ();
                    switch (true) {
                        case strpos ( $array [$coordskey], "[" ) !== false :
                            // geojson
                            $coords_decoded = json_decode ( $array [$coordskey] );
                            if (isset ( $coords_decoded->type )) {
                                // geometry object
                                $obj_geometry = $coords_decoded;
                            } else {
                                // geometry.coordinates array
                                if (isset ( $coords_decoded [0] [0] ) && is_array ( $coords_decoded [0] [0] )) {
                                    // check if first element is the same as the
                                    // last
                                    if ($coords_decoded [0] [0] == $coords_decoded [0] [count ( $coords_decoded [0] ) - 1]) {
                                        // check if there is > 1 root level(case
                                        // of holes or multipolygon)
                                        if (isset ( $coords_decoded [0] [1] )) {
                                            if (isset ( $coords_decoded [0] [0] [0] [0] )) {
                                                $type = "MultiPolygon";
                                            } else {
                                                $type = "Polygon"; // multipolygon
                                            }
                                        } else {
                                            $type = "Polygon";
                                        }
                                    } else {
                                        $type = "MultiLineString";
                                    }
                                } else {
                                    if (is_array ( $coords_decoded [0] )) {
                                        $type = "LineString"; // or MultiPoint
                                    } else {
                                        $type = "Point";
                                    }
                                }
                                $obj_geometry->type = $type;
                                $obj_geometry->coordinates = $coords_decoded;
                            }
                            break;
                        default :
                            $arr_polygons = array ();
                            if (strpos ( $array [$coordskey], "|" ) !== false) {
                                $obj_geometry->type = "MultiPolygon";
                                $arr_polygons = explode ( "|", $array [$coordskey] );
                            } else {
                                $obj_geometry->type = "Polygon";
                                $arr_polygons [] = $array [$coordskey];
                            }
                            $counter_polygon = 0;
                            foreach ( $arr_polygons as $polygon ) {
                                $arr_coords_strings = explode ( " ", $polygon );
                                $arr_coords_strings = array_filter ( $arr_coords_strings );
                                foreach ( $arr_coords_strings as $arr_coords_string ) {
                                    if (strpos ( $array [$coordskey], ":" ) !== false) {
                                        $arr_coords = explode ( ":", $arr_coords_string );
                                    } else {
                                        $arr_coords = explode ( ",", $arr_coords_string );
                                    }
                                    $arr_latlong = array ();
                                    $arr_latlong [0] = $arr_coords [0];
                                    $arr_latlong [1] = $arr_coords [1];

                                    switch ($obj_geometry->type) {
                                        case "Polygon" :
                                            $arr_coordinates [$counter_polygon] [] = $arr_latlong;
                                            break;
                                        case "MultiPolygon" :
                                            $arr_coordinates [$counter_polygon] [0] [] = $arr_latlong;
                                            break;
                                    }
                                }
                                $counter_polygon ++;
                            }
                            $obj_geometry->coordinates = $arr_coordinates;
                            break;
                    }
                    unset($array[$coordskey]);
                    $name = self::xmlgetelement($array);
                    $extendeddata = self::getExtendedDataElement($array);
                } else {
                    self::printArray($array, $placemarks);
                }

                if (($lat != "" && $long != "") || isset ( $obj_geometry->coordinates[0] )) {
                    echo "<Placemark><name>$key</name><description>" . $name . "</description>";
                    echo $extendeddata;

                    if ($lat != "" && $long != "") {
                        echo "<Point><coordinates>" . $long . "," . $lat . "</coordinates></Point>";
                    }

                    if(isset($obj_geometry) && $obj_geometry->type == 'Point'){
                        echo "<Point><coordinates>" . $obj_geometry->coordinates[0] . "," . $obj_geometry->coordinates[1] . "</coordinates></Point>";
                    }

                    if(isset ( $obj_geometry->type )){
                        if ($obj_geometry->type == 'Polygon') {
                            self::printPolygon($obj_geometry->coordinates);
                        } elseif ($obj_geometry->type == 'MultiPolygon') {
                            echo "<MultiGeometry>";
                            foreach ( $obj_geometry->coordinates  as $arr_polygon ) {
                                self::printPolygon($arr_polygon);
                            }
                            echo "</MultiGeometry>";
                        } elseif($obj_geometry->type == 'Polyline'){
                            echo self::_printPolyLine($obj_geometry->coordinates);
                        } elseif($obj_geometry->type == 'MultiPolyline'){
                            echo "<MultiGeometry>";
                            foreach ( $obj_geometry->coordinates  as $coordinates ) {
                                echo self::_printPolyLine($coordinates);
                            }
                            echo "</MultiGeometry>";
                        } elseif($obj_geometry->type == "MultiLineString"){

                            $lineStrings = "";

                            foreach($obj_geometry->coordinates as $lineString){
                                $lineStrings .= self::_printLineString($lineString);
                            }

                            echo "<MultiGeometry>$lineStrings</MultiGeometry>";
                        } elseif ($obj_geometry->type == 'LineString'){
                            echo self::_printLineString($obj_geometry->coordinates);
                        }
                    }

                    echo "</Placemark>";
                }
            }
        }
    }

    private static function _printLineString(array $geo)
    {
        $coordinates = "";
        foreach($geo as $coords){
            $coordinates .= implode(',',  $coords).' ';
        }

        return "<LineString><coordinates>$coordinates</coordinates></LineString>";
    }

    private static function printPolygon($arr_coordinates){
        echo "<Polygon>";
        foreach ( $arr_coordinates  as $index =>  $arr_polygon ) {
            if($index == 0){
                echo "<outerBoundaryIs>";
            }else{
                echo "<innerBoundaryIs>";
            }
            echo "<LinearRing><coordinates>";
            foreach ( $arr_polygon as $arr_latlong ) {
                echo $arr_latlong [0] . "," . $arr_latlong [1] . " ";
            }
            echo "</coordinates></LinearRing>";
            if($index == 0){
                echo "</outerBoundaryIs>";
            }else{
                echo "</innerBoundaryIs>";
            }
        }
        echo "</Polygon>";
    }

    protected static function _printPolyLine(array $coordinates)
    {
        $coordinatesString = "";

        foreach ($coordinates as $coordinate){
            foreach($coordinate as $latLng){
                $coordinatesString .= "$latLng[0],$latLng[1] ";
            }
        }

        $xmlString = "
            <LineString>
                <altitudeMode>relative</altitudeMode>
                <coordinates>
                $coordinatesString
                </coordinates>
            </LineString>
        ";

        return $xmlString;
    }

    /**
     * Create the geo graphical placemarks in kml
     * Currently only properties that are not nested are picked up.
     */
    private static function getArray($dataObj, $geo)
    {

        $body = "";

        $data = $dataObj->data;

        foreach ($data as $key => $value) {

            if (is_array($value)) {
                $entry = $value;
            } elseif (is_object($value)) {
                $entry = get_object_vars($value);
            }

            // We assume that if longitude exists, latitude does as well if the geometry is a single point
            // A point can either be a single column value, or split up in a latitude and longitude
            $geo_type = 'point';
            $is_point = (count($geo) > 1) || !empty($geo['point']);

            if (!$is_point) {
                $geo_type = key($geo);
                $column_name = $geo[$geo_type];
            }

            if (!empty($entry)) {

                $name = htmlspecialchars($key);
                if (!empty($entry['name'])) {
                    $name = $entry['name'];
                }
                $extendeddata = self::getExtendedDataElement($entry);

                $description = "";
                if (!empty($key) && is_numeric($key)) {
                    $description = "<![CDATA[<a href='" . \URL::to($dataObj->definition['collection_uri'] . '/' . $dataObj->definition['resource_name']) . '/' .  htmlspecialchars($key)  . ".map'>". \URL::to($dataObj->definition['collection_uri'] . '/' . $dataObj->definition['resource_name']) . '/' .  htmlspecialchars($key) ."</a>]]>";
                }

                $body .= "<Placemark><name>".$name."</name><description>".$description."</description>";
                $body .= $extendeddata;
                if ($is_point) {

                    if (count($geo) > 1) {
                        $point = $entry[$geo['longitude']] . ',' . $entry[$geo['latitude']];
                    } else {
                        $point = $entry[$geo['point']];
                    }

                    $body .= "<Point><coordinates>" . $point . "</coordinates></Point>";
                } else {
                    if ($geo_type == 'polyline') {

                        $body .= "<MultiGeometry>";
                        foreach (explode(';', $entry[$geo['polyline']]) as $coord) {
                            $body .= "<LineString><coordinates>".$coord."</coordinates></LineString>";
                        }
                        $body .= "</MultiGeometry>";

                    } elseif ($geo_type == 'polygon') {
                        $body .= "<Polygon><outerBoundaryIs><LinearRing><coordinates>". $entry[$geo['polygon']] ."</coordinates></LinearRing></outerBoundaryIs></Polygon>";
                    } else {
                        \App::abort(500, "The geo type, $geo_type, is not supported. Make sure the (combined) geo type is correct. (e.g. latitude and longitude are given).");
                    }
                }
                $body .= "</Placemark>";
            }
        }

        return $body;
    }

    /**
     * Case insensitive version of array_key_exists.
     * Returns the matching key on success, else false.
     *
     * @param string $key
     * @param array $search
     * @return string|false
     */
    private static function keyExists($key, $search)
    {

        if (array_key_exists($key, $search)) {
            return $key;
        }

        if (!(is_string($key) && is_array($search) && count($search))) {
            return false;
        }

        $key = strtolower($key);

        foreach ($search as $k => $v) {
            if (strtolower($k) == $key) {
                return $k;
            }
        }
        return false;
    }


    public static function getDocumentation()
    {
        return "Returns a KML file with geo properties of the data.";
    }

    /**
     * @param mixed $coordsKey
     * @return $this
     */
    public static function setCoordsKey($coordsKey)
    {
        self::$_coordsKey = $coordsKey;
    }

    /**
     * @return mixed
     */
    public static function getCoordsKey()
    {
        return self::$_coordsKey;
    }

    /**
     * @param mixed $latKey
     * @return $this
     */
    public static function setLatKey($latKey)
    {
        self::$_latKey = $latKey;
    }

    /**
     * @return mixed
     */
    public static function getLatKey()
    {
        return self::$_latKey;
    }

    /**
     * @param mixed $lngKey
     * @return $this
     */
    public static function setLngKey($lngKey)
    {
        self::$_lngKey = $lngKey;
    }

    /**
     * @return mixed
     */
    public static function getLngKey()
    {
        return self::$_lngKey;
    }


}
