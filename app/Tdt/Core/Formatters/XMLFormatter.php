<?php

namespace Tdt\Core\Formatters;


define("NUMBER_TAG_PREFIX", "_");
define("DEFAULT_ROOTNAME", "data");

/**
 * XML Formatter
 * @copyright (C) 2011, 2014 by OKFN Belgium vzw/asbl
 * @license AGPLv3
 * @author Michiel Vancoillie <michiel@okfn.be>
 */
class XMLFormatter implements IFormatter
{

    public static function createResponse($dataObj)
    {
        // Create response
        $response = \Response::make(self::getBody($dataObj), 200);

        // Set headers
        $response->header('Content-Type', 'text/xml;charset=UTF-8');

        return $response;
    }

    public static function getBody($dataObj)
    {
        $definition = $dataObj->definition;

        // Create Dataset XML root element with meta-data
        $datasetXML = new \SimpleXMLElement('<dataset></dataset>');
        $datasetXML->addAttribute('id', $definition['resource_name']);
        $datasetXML->addAttribute('collection', $definition['collection_uri']);
        $datasetXML->addAttribute('created_at', $definition['created_at']);
        $datasetXML->addAttribute('updated_at', $definition['updated_at']);
        isset($definition['title'])     ?   $datasetXML->addAttribute('title', $definition['title']) : null;
        isset($definition['source'])    ?   $datasetXML->addAttribute('source', $definition['source']) : null;

        // Add paging to XML
        if(property_exists($dataObj, 'paging') && is_array($dataObj->paging) && $dataObj->paging){
            $paging = $dataObj->paging;
            if(array_key_exists('pages', $paging) && array_key_exists('pageCurrent', $paging) && array_key_exists('pageNext', $paging) && array_key_exists('pagePrev', $paging)){
                $pagingXML = $datasetXML->addChild('paging');
                $pagingXML->addChild('pages', $paging['pages']);
                $pagingXML->addChild('pageCurrent', $paging['pageCurrent']);
                $pagingXML->addChild('pageNext', $paging['pageNext']);
                $pagingXML->addChild('pagePrev', $paging['pagePrev']);
                $pagingXML->addChild('pageSize', count($dataObj->data));
            }
        }

        // Add data to XML
        $dataXML = $datasetXML->addChild('data');
        $dataXML->addAttribute('records', count($dataObj->data));

        foreach($dataObj->data as $recordObj){
            $record = $dataXML->addChild('record');
            $recordObj = (array) $recordObj;

            foreach($recordObj as $field => $value){
                if(is_numeric($field))
                    $field = 'i'.$field;
                try{
                    $record->addChild($field, htmlspecialchars(self::getXMLString($value)));
                }catch(\Exception $e){
                    echo $value;
                    echo $field;
                    echo $e->getMessage();die();
                }
            }
        }

        return $datasetXML->asXML();

        // Rootname equals resource name
        $rootname = $dataObj->definition['resource_name'];

        // Check for semantic source
        if ($dataObj->is_semantic) {

            // Check if a configuration is given
            $conf = array();
            if (!empty($dataObj->semantic->conf)) {
                $conf = $dataObj->semantic->conf;
            }

            return $dataObj->data->serialise('rdfxml');
        }

        // Build the body
        $body = '<?xml version="1.0" encoding="UTF-8" ?><'.$rootname.' created_at="'.$dataObj->definition['created_at'].'">';
        $body .= self::transformToXML($dataObj->data, $rootname);
        $body .= '</'.$rootname.'>';

        return $body;
    }

    private static function transformToXML($data, $nameobject)
    {
        // Set the tagname, replace whitespaces with an underscore
        $xml_tag = str_replace(' ', '_', $nameobject);

        // Start an empty object to add to the document
        $object = '';

        if (is_array($data) && self::isAssociative($data)) {

            $object = "<$xml_tag>";

            // Check for attributes
            if (!empty($data['@attributes'])) {

                $attributes = $data['@attributes'];

                if (is_array($attributes) && count($attributes) > 0) {
                    // Trim last '>'
                    $object = rtrim($object, '>');

                    // Add attributes
                    foreach ($attributes as $name => $value) {
                        $object .= " $name='".$value."'";
                    }

                    $object .= '>';
                }
            }

            unset($data['@attributes']);

            // Data is an array (translates to elements)
            foreach ($data as $key => $value) {

                // Check for special keys, then add elements recursively
                if ($key === '@value') {
                    $object .= self::getXMLString($value);
                } elseif (is_numeric($key)) {
                    $object .= self::transformToXML($value, 'i' . $key);
                } elseif ($key == '@text') {
                    if (is_array($value)) {
                        $object .= implode(' ', $value);
                    } else {
                        $object .= $value;
                    }
                } else {
                    $object .= self::transformToXML($value, $key);
                }
            }

            // Close tag
            $object .= "</$xml_tag>";

        } elseif (is_object($data)) {

            $object .= "<$xml_tag>";

            $data = get_object_vars($data);

            // Data is object
            foreach ($data as $key => $element) {
                if (is_numeric($key)) {
                    $object .= self::transformToXML($element, 'i' . $key);
                } else {
                    $object .= self::transformToXML($element, $key);
                }
            }

            // Close tag
            $object .= "</$xml_tag>";

        } elseif (is_array($data)) {

            // We have a list of elements
            foreach ($data as $key => $element) {

                if (is_numeric($key)) {
                    $object .= self::transformToXML($element, $xml_tag);
                } else {
                    $object .= self::transformToXML($element, $key);
                }
            }

        } else {

            $object .= "<$xml_tag>";
            // Data is string append it
            $object .= self::getXMLString($data);
            $object .= "</$xml_tag>";
        }



        return $object;
    }

    private static function getXMLString($string)
    {

        // Check for XML syntax to escape
        if (preg_match('/[<>&]+/', $string)) {
            $string = '<![CDATA[' . $string . ']]>';
        }

        return $string;
    }

    private static function isAssociative($arr)
    {
        return (bool)count(array_filter(array_keys($arr), 'is_string'));
    }

    public static function getDocumentation()
    {
        return "Prints plain old XML. Watch out for tags starting with an integer: an underscore will be added.";
    }
}
