<?php

/**
 * Buurtmonitor Definition Model
 *
 * @copyright (C) 2011, 2014 by District01 Belgium
 * @license AGPLv3
 * @author Arne Van den Bogaert
 */
class BuurtmonitorDefinition extends SourceType
{
    protected $table = 'buurtmonitordefinitions';

    protected $fillable = array('uri', 'guid', 'description');

}